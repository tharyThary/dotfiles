export ZSH="$HOME/.oh-my-zsh"

#THEME
ZSH_THEME="amuse"

#PLUGINS
plugins=(git)

source $ZSH/oh-my-zsh.sh

#ALIASES
alias lvim="//home/thary/.local/bin/lvim"
alias nf="neofetch"
alias ff="fastfetch"
alias sf="sysfetch"
alias update="sudo pacman -Syu --noconfirm"

#GIT ALIASES
alias g="git"
alias gc="git clone"
alias gcm="git commit"
alias gcmm="git commit -m"
alias ga="git add"
alias gaa="git add -A"
alias gap="git add ."
alias gb="git branch"
alias gbs="git switch"
alias gp="git push"
alias gpm="git push --mirror"
alias gpl="git pull"

#START AFTER ZSH
colorscript -e square

#SET CURSOR
echo -ne '\e[5 q'
