## Thary's dotfiles

### My programms configs:

1. [NeoVIm](/.config/nvim) (Text editor)
2. [Zsh](/.zshrc) (Shell)
3. [Bspwm](/.config/bspwm) (Windowws manager)
4. [Sxhkd](/.config/sxhkd) (Program for hotkeys)
5. [Alacritty](/.config/alacritty) (Terminal)
6. [Rofi](/.config/rofi) (App list)
7. [Sudo](/etc/sudoers)
8. [Neofetch](/.config/neofetch) (System info)


![My bswpm desktop with neofetch](/Screenshots/1.png)
*More screenshots in folder ["Screenshots"](/Screenshots)*

---
## Sxhkd hotkeys
1. *Open terminal* - super + enter
2. *Open browser* - super + b
3. *Open Telegram* - super + t
4. *Open E-Mail client* - super + m
5. *Open Freetube* - super + f
6. *Open Rofi* (app menu) - alt + space
7. *Change desktop* - super + 1-9
8. *Send window to desktop* ... - super + shift + 1-9
9. *Reload sxhkd* - super + alt + r
10. *Reload bspwm* - super + r
11. *Close app* - super + q
12. *Kill app* - super + shift +q
13. *Expand window* - super + shift/alt + arrows
14. *Monocle mod* - super + tab
15. *Floating mod* - super + shift + tab
16. *Fullscreen mod* - super + f11
17. *Make window biggest* - super + shift + f11
18. *Change window position* - super + a,s,w,d
19. *Select window* - super + arrows
20. *Make screenshot* - PrintScr
21. *Change border width* - super + shift + alt + 1-5
22. *Focus on last window* - alt + tab
23. *Of/on gaps* - super + {-} or {+}
24. *Mute audio* - XF86AudioMute
25. *Up volume* - XF86AudioRaiseVolume
25. *Dowm volume* - XF86AudioLowerVolume
26. *Mute micro* - XF86AudioMicMute
