**Rofi** is popular app menu

![Image](https://codeberg.org/Thary/Bspwm/src/branch/main/Rofi/rofi.png)

*This Rofi config is taked from https://github.com/EndeavourOS-Community-Editions/bspwm/tree/main/.config/rofi*
