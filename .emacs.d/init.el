;;; init.el --- Summary
;;; Commentary:
;;; Settings for GNU EMACS.
;;; Code:
(provide 'init)

(global-display-line-numbers-mode) ;; Выводить номер строки

(fset 'yes-or-no-p 'y-or-n-p) ;; Упрощение подтверждения действия

(setq-default cursor-type 'bar) ;; Установка курсора

;; Заставляем use-package устанавливать пакеты
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa"  . "https://elpa.gnu.org/packages/"))) ;; Подключение архивов с пакетами

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Пакеты
(use-package nord-theme)
(use-package rainbow-delimiters)
(use-package yasnippet)
(use-package web-mode)

;; Настройка web-mode
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode)) ;; Автоматически вкючать web-mode в .html
(setq web-mode-enable-current-element-highlight t)

(auto-fill-mode 0) ;; Выключить auto-fill-mode

;; Включение IDO
(require 'ido)
(ido-mode t)

(require 'tramp) ;; Включить удаленное редактирование документов

;; Включение темы
(load-theme 'nord t)

;; Zooming
(global-set-key (kbd "C-+") 'text-scale-increase) ;; Увеличить размер шрифта
(global-set-key (kbd "C--") 'text-scale-decrease) ;; Уменьшить размер шрифта

;; Работа с буферами
(global-set-key (kbd "C-<next>") 'next-buffer) ;; Перейти в следующий буфер 
(global-set-key (kbd "C-<prior>") 'previous-buffer) ;; Перейти в предыдущий буфер
(global-set-key (kbd "C-a") 'mark-whole-buffer) ;; Выделить всё
(global-set-key (kbd "C-o") 'dired) ;; Запустить dired-mode
(global-set-key (kbd "C-r") 'revert-buffer) ;; Обновить буфер

;; Работа с файлом
(global-set-key (kbd "C-s") 'save-buffer)  ;; Сохранить файл
(global-set-key (kbd "C-S-s") 'write-file) ;; Сохранить файл под другим именем

(setq-default
 create-lockfiles 0 ;; Отключить блокирование файлов
 inhibit-splash-screen t ;; Не показывать заставку
 inhibit-startup-message t ;; Не показывeать сообщение при запуске
 initial-major-mode 'fundamental-mode ;; Использовать fundamental-mode для новых буферов
 initial-scratch-message "" ;; Сообщение в новых пустых буферах
 make-backup-files nil ;; Не создавать резервные копии редактируемых файлов
 truncate-lines 1 ;; Переносить длинные строки
 user-email-address "thary@riseup.net" ;; Ваш email
 user-full-name "Thary") ;; Ваше полное имя

;; Разрешить редактирование файлов в ~/.emacs.d
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("37768a79b479684b0756dec7c0fc7652082910c37d8863c35b702db3f16000f8" default))
 '(package-selected-packages
   '(yasnippet-snippets evil-visual-mark-mode evil-mode use-package nord-theme))
 '(safe-local-variable-val ues)
 '(safe-local-variable-values '((git-commit-major-mode . git-commit-elisp-text-mode)))
 '(warning-suppress-types '((use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(cua-mode 0) ;; Отключение Ctrl+C, Ctrl-V и т. д.

;; Скрыть лишние элементы
(menu-bar-mode 1) ;; Скрыть/показать меню
(scroll-bar-mode 0) ;; Скрыть полосы прокрутки
(tool-bar-mode 0) ;; Скрыть панель кнопок
(tooltip-mode 0) ;; Не выводить подсказки
(window-divider-mode 0) ;; Не показывать границу окон

;; Сохранять состояние
(defvar desktop-save t "Save desktop settings without any questions")
(setq desktop-save t)
(desktop-save-mode 1)

;;; init.el ends here
